var mongoose = require('mongoose');

exports.connect = function(url) {  
    const conn = mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true })
            .then(() =>  console.log('connection succesful'))
            .catch((err) => console.error(err));
}    

