var router = require('express').Router();

    const User = require('./UserRequestHandler');

    // Create a new User
    router.post('/', User.create);

    // Retrieve all User
    router.get('/', User.findAll);

    // Login User
    router.post('/login', User.login);

    // Retrieve a single Note with noteId
    // app.get('/notes/:noteId', notes.findOne);

    // Update a Note with noteId
    // app.put('/notes/:noteId', notes.update);

    // Delete a Note with noteId
    // app.delete('/notes/:noteId', notes.delete);


module.exports = router;