var User = require('./UserModel');
var Success = require('../success');
const crypto = require('../../utils/crypto');

exports.findAll = (req, res) => { 
    User.find({}, function(err, data){
        console.log(data);
    })
}

exports.login = (req, res) => {
    const email = req.body.email;
    User.findOne({'email' : email}, function(err, data){
        console.log('User found ');
        // In case the user not found   
        if(err) {
          console.log('THIS IS ERROR RESPONSE')
          res.json(err)
        } 
        const hashPassword = crypto.decrypt(data.password);
        if (data && hashPassword === req.body.password){
          console.log('User and password is correct')
          const response = {
              firstname : data.firstname,
              lastname : data.lastname,
              email : data.email
          }
          res.json(response);
        } else {
          console.log("Credentials wrong");
          res.json({data: "Login invalid"});
        }          
    })
}

exports.create = (req,res) => {
    const hashPassword = crypto.encrypt(req.body.firstname);
    const user = new User({
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        email: req.body.email,
        password: hashPassword
    })
    user.save();
    res.send(Success);
    // .then(data => {
    //     res.send(data);
    // }).catch(err => {
    //     res.status(500).send({
    //         message: err.message || "Some error occurred while creating the Note."
    //     });
    // });
    console.log("Hey");
}


