import React from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import Login from './components/login';
import Register from './components/register';
import AuthService from './auth/authentication';

class App extends React.Component{
  authService;
  constructor(){
    super();
    this.authService = new AuthService();
  }
  render(){
    return (
      <div className="App">
        <Router>
          <Switch>
            <Route exact path='/'>{ this.authService.isAuthenticated() ? <Redirect to='/dashboard' /> : <Redirect to='/login' />}</Route>
            <Route path='/login' component={Login}></Route>
            <Route path='/register' component={Register}></Route>
          </Switch>
        </Router>
      </div>  
    );
  }
}


export default App;
