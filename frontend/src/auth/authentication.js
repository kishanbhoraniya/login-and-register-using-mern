class AuthService {
    loggedin = true
    constructor(){
        if (sessionStorage.getItem('user') !== null) {
            this.user = JSON.parse(sessionStorage.getItem('user'));
            this.loggedIn = true;
        } else if (localStorage.getItem('user') !== null) {
            this.user = JSON.parse(localStorage.getItem('user'));
            this.loggedIn = true;
        } 
    }
    isAuthenticated(){
        return this.loggedIn;
    }
}
export default AuthService;

